import "dart:async";
import "package:rxdart/rxdart.dart";
import "../store.dart";

class CounterBloc {
  CounterBloc() {
    _actionController.stream.listen(_increaseStream);
  }

  final _counterStream = BehaviorSubject<int>.seeded(AppIStoreData().counter);

  Stream get pressedCount => _counterStream.stream;
  Sink get _addValue => _counterStream.sink;

  StreamController _actionController = StreamController();
  StreamSink get incrementCounter => _actionController.sink;

  void _increaseStream(data) async {
    int _counter = AppIStoreData().counter + data;
    _addValue.add(_counter);
    await saveCounter(_counter);
  }

  void dispose() {
    _counterStream.close();
    _actionController.close();
  }
}
