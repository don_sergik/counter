import "package:flutter/material.dart";
import "store.dart";

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    _getCounter();
  }

  Future _getCounter() async {
    await readCounter();
    Navigator.of(context).pushReplacementNamed("/counter");
  }

  @override
  Widget build(BuildContext context) {
    return Container(color: Colors.black);
  }
}
