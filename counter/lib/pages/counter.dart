import "package:flutter_local_notifications/flutter_local_notifications.dart";
import "package:flutter/services.dart";
import "package:flutter/material.dart";
import "../bloc/counter-bloc.dart";
import "../ui/rounded_button.dart";
import "../main.dart";
import "operation.dart";

class CounterPage extends StatefulWidget {
  @override
  _CounterPageState createState() => _CounterPageState();
}

class _CounterPageState extends State<CounterPage> {
  final CounterBloc counterBloc = CounterBloc();
  final MethodChannel platform = MethodChannel("crossingthestreams.io/resourceResolver");

  @override
  void initState() {
    super.initState();
    _requestIOSPermissions();
    _configureSelectNotificationSubject();
  }

  @override
  void dispose() {
    didReceiveLocalNotificationSubject.close();
    selectNotificationSubject.close();
    super.dispose();
  }

  void _requestIOSPermissions() {
    flutterLocalNotificationsPlugin.resolvePlatformSpecificImplementation<IOSFlutterLocalNotificationsPlugin>()
      ?.requestPermissions(
        alert: true,
        badge: true,
        sound: true,
      );
  }

  void _configureSelectNotificationSubject() {
    selectNotificationSubject.stream.listen((String payload) async {
      await Navigator.of(context).push(
        MaterialPageRoute(builder: (BuildContext context) => OperationPage(value: int.parse(payload)))
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Counter"),
      ),
      backgroundColor: Colors.grey[200],
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            StreamBuilder<int>(
              stream: counterBloc.pressedCount,
              builder: (context, snapshot) {
                counterBloc.incrementCounter.add(0);
                return Text("Счетчик = ${snapshot.data.toString()}", style: TextStyle(fontSize: 18));
              }
            ),
            SizedBox(height: 64),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RoundedButton(
                  color: Colors.red,
                  title: "-1",
                  onPressed: () {
                    counterBloc.incrementCounter.add(-1);
                  }
                ),
                SizedBox(width: 24),
                RoundedButton(
                  color: Colors.green,
                  title: "+1",
                  onPressed: () {
                    counterBloc.incrementCounter.add(1);
                  }
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
