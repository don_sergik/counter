import "package:flutter/material.dart";
import "../ui/rounded_button.dart";
import "../store.dart";

class OperationPage extends StatefulWidget {
  OperationPage({this.value});

  int value;

  @override
  _OperationPageState createState() => _OperationPageState();
}

class _OperationPageState extends State<OperationPage> {
  _cancelAction() {
    Navigator.pop(context);
  }

  Future<void> _okAction() async {
    await saveCounter(widget.value);
    Navigator.pop(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Operation"),
      ),
      backgroundColor: Colors.green[100],
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              "Текущее значение счетчика: ${AppIStoreData().counter}\n"+
              "Новое значение счетчика: ${widget.value}",
              style: TextStyle(fontSize: 18),
            ),
            SizedBox(height: 64),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                RoundedButton(
                  color: Colors.red,
                  title: "Cancel",
                  onPressed: () {
                    _cancelAction();
                  }
                ),
                SizedBox(width: 24),

                RoundedButton(
                  color: Colors.green,
                  title: "Ok",
                  onPressed: () {
                    _okAction();
                  }
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
