import "package:shared_preferences/shared_preferences.dart";

const TIMER_INTERVAL = 30;

class AppIStoreData {
  int counter = 0;

  AppIStoreData._();
  static final AppIStoreData _singleton = new AppIStoreData._internal();
  factory AppIStoreData() {
    return _singleton;
  }
  AppIStoreData._internal();
}

Future<int> readCounter() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  AppIStoreData().counter = prefs.getInt("counter") ?? 0;
  return AppIStoreData().counter;
}

Future saveCounter(int counter) async {
  AppIStoreData().counter = counter;
  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.setInt("counter", AppIStoreData().counter);
}
