import "dart:async";
import "dart:math";
import "package:flutter_local_notifications/flutter_local_notifications.dart";
import "package:rxdart/subjects.dart";
import "package:flutter/material.dart";
import "pages/counter.dart";
import "pages/operation.dart";
import "splash_screen.dart";
import "store.dart";

StreamSubscription periodicTimer;
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
final BehaviorSubject<ReceivedNotification> didReceiveLocalNotificationSubject = BehaviorSubject<ReceivedNotification>();
final BehaviorSubject<String> selectNotificationSubject = BehaviorSubject<String>();
NotificationAppLaunchDetails notificationAppLaunchDetails;

class ReceivedNotification {
  final int id;
  final String title;
  final String body;
  final String payload;

  ReceivedNotification({
    @required this.id,
    @required this.title,
    @required this.body,
    @required this.payload,
  });
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  notificationAppLaunchDetails =  await flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails();

  var initializationSettingsAndroid = AndroidInitializationSettings("app_icon");
  var initializationSettingsIOS = IOSInitializationSettings(
      requestAlertPermission: false,
      requestBadgePermission: false,
      requestSoundPermission: false,
      onDidReceiveLocalNotification: (int id, String title, String body, String payload) async {
        didReceiveLocalNotificationSubject.add(ReceivedNotification(id: id, title: title, body: body, payload: payload));
    });
  var initializationSettings = InitializationSettings(initializationSettingsAndroid, initializationSettingsIOS);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings, onSelectNotification: (String payload) async {
    selectNotificationSubject.add(payload);
  });
  runApp(App());
}

var appRoutes = <String, WidgetBuilder> {
  "/"            : (BuildContext context) => SplashScreen(),
  "/counter"     : (BuildContext context) => CounterPage(),
  "/operation"   : (BuildContext context) => OperationPage(),
  "splash_screen": (BuildContext context) => SplashScreen(),
};

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  Random random = new Random();
  static  Timer _timer;

  @override
  void initState() {
    super.initState();
    periodicTimer = new Stream.periodic(const Duration(seconds: TIMER_INTERVAL))
      .listen((_) {
        bool _isFirst = random.nextInt(100) > 50 ? true : false;
        int value = random.nextInt(_isFirst ? 10 : 1000);
        String _body = _isFirst ? "Изменить счетчик на $value" : "Установить счетчик в $value";
        showNotification(null, _body, _isFirst ? AppIStoreData().counter + value : value);
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  Future<void> showNotification(String title, String body, int value) async {
    var androidPlatformChannelSpecifics = AndroidNotificationDetails(
      "your channel id", "your channel name", "your channel description",
      importance: Importance.Max, priority: Priority.High, ticker: "ticker"
    );
    var iOSPlatformChannelSpecifics = IOSNotificationDetails();
    var platformChannelSpecifics = NotificationDetails(androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);
    await flutterLocalNotificationsPlugin.show(0, title, body, platformChannelSpecifics, payload: "$value");
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      initialRoute: "/",
      routes: appRoutes,
    );
  }
}
